import webiopi
import datetime
from webiopi.utils import types

GPIO = webiopi.GPIO
LIGHT = 23
SENSOR = 27
 
def setup():
	GPIO.setFunction(LIGHT, GPIO.OUT)
	GPIO.setFunction(SENSOR, GPIO.IN, GPIO.PUD_DOWN)

def loop():
	webiopi.sleep(5)

def destroy():
	GPIO.digitalWrite(LIGHT, GPIO.LOW)

def activate():
	GPIO.digitalWrite(LIGHT, GPIO.HIGH)
	webiopi.sleep(2)
	GPIO.digitalWrite(LIGHT, GPIO.LOW)

def isOpen():
	open = GPIO.digitalRead(SENSOR)
	print("Current state is:")
	if open is True:
		print("OPEN")
	else:
		print("CLOSED")
#returns true if voltage is high
	return open

@webiopi.macro
def getState():
	json = {}
	if isOpen() is False:
		json['closed'] = True
	else:
		json['closed'] = False
	return types.jsonDumps(json)

@webiopi.macro
def openDoor():
	if isOpen() is False:
		print("OPENING")
		#json = getState()
		activate()
	return types.jsonDumps({"action":"Opening"})

@webiopi.macro
def closeDoor():
	if isOpen() is True:
		print("CLOSING")
		#json = getState()
		activate()
	return types.jsonDumps({"action":"Closing"})