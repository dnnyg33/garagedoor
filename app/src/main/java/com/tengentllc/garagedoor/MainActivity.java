package com.tengentllc.garagedoor;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.getpebble.android.kit.PebbleKit;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {


    private static final String TAG = MainActivity.class.getSimpleName();
    public static final int GEOFENCE_RADIUS = 350;

    @Bind(R.id.progress_indicator)
    ProgressBar progressBar;

    @Bind(R.id.open_button)
    Button mOpenButton;
    @Bind(R.id.close_button)
    Button mCloseButton;
    @Bind(R.id.statebtn)
    Button mState;
    @Bind(R.id.state)
    TextView mStateTv;

    @Bind(R.id.logs)
    TextView logs;
    private GoogleApiClient mGoogleApiClient;

    private PendingIntent mGeofencePendingIntent;

    private String[] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permissions[0]) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permissions, 1099);
            }
        }

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        final retrofit.Callback logCallback = new retrofit.Callback() {
            @Override
            public void onResponse(final retrofit.Response response, Retrofit retrofit) {
                runOnUiThread(getUILogRunnable(response));
                getState();
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        };


        mOpenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                GarageApi.getApi().openDoor().enqueue(logCallback);
            }
        });
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                GarageApi.getApi().closeDoor().enqueue(logCallback);
            }
        });

        mState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStateTv.setText("Checking...");
                getState();
            }
        });

        getState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isConnected = PebbleKit.isWatchConnected(this);
        Toast.makeText(this, "Pebble " + (isConnected ? "is" : "is not") + " connected!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1099:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                }
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private Runnable getUILogRunnable(final retrofit.Response response){
        return new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                String responseString = response.raw().request().urlString().substring(34) + " -" + response.message() + " at " + response.headers().get("Date");
                logs.setText(responseString + "\n" + logs.getText() );
            }
        };
    }

    private void getState() {
        progressBar.setVisibility(View.VISIBLE);
        GarageApi.getApi().getState().enqueue(new retrofit.Callback<DoorState>() {
            @Override
            public void onResponse(retrofit.Response<DoorState> response, Retrofit retrofit) {
                if (response.body() != null) {
                    mStateTv.setText("Current State: " + response.body().currentState());
                    if (response.body().currentState().equals("Closed")) {
                        setBorderColors(false);
                    } else {
                        setBorderColors(true);
                    }
                } else {
                    mStateTv.setText("Error");
                }
                runOnUiThread(getUILogRunnable(response));
            }

            @Override
            public void onFailure(Throwable t) {
                logs.setText(t.getLocalizedMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void setBorderColors(boolean open) {
        if (open) {
            mOpenButton.setTextColor(getResources().getColor(R.color.red));
            mCloseButton.setTextColor(getResources().getColor(R.color.black));

        } else {
            mCloseButton.setTextColor(getResources().getColor(R.color.red));
            mOpenButton.setTextColor(getResources().getColor(R.color.black));

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.registerGeoFence).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startLocationUpdates();
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    ////////////GEOFENCE

//    private Geofence createGeoFenceAuto() {
//        return new Geofence.Builder()
//        .setRequestId("com.tengentllc.home_auto")
//                .setCircularRegion(ProtectedValues.LATITUDE, ProtectedValues.LONGITUDE, GEOFENCE_RADIUS)
//                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
//                .setExpirationDuration(Geofence.NEVER_EXPIRE)
//                .build();
//    }

    private Geofence createGeoFenceAsk() {
//        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        return new Geofence.Builder()
                .setRequestId("com.tengentllc.home_ask")
                .setCircularRegion(ProtectedValues.LATITUDE, ProtectedValues.LONGITUDE, GEOFENCE_RADIUS * 2)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .build();
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
//        builder.addGeofence(createGeoFenceAuto());
        builder.addGeofence(createGeoFenceAsk());
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);

        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }



    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }


    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }




    //////////////////FUSE API
    @Override
    public void onConnected(Bundle bundle) {
//        startLocationUpdates();
    }

    private void startLocationUpdates() {
        LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, getGeofencingRequest(),
                getGeofencePendingIntent()).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(Status status) {
        Log.d(TAG, "onResult: "+status.getStatusMessage());
    }
}
