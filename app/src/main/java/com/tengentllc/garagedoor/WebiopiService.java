package com.tengentllc.garagedoor;

import com.squareup.okhttp.Response;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by dgeorge on 1/30/16.
 */
public interface WebiopiService {

    @POST("/macros/openDoor")
    Call<DoorAction> openDoor();


    @POST("/macros/closeDoor")
    Call<DoorAction> closeDoor();

    @POST("/macros/getState")
    Call<DoorState> getState();
}
