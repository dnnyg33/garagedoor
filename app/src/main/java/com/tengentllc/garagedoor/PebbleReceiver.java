package com.tengentllc.garagedoor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.getpebble.android.kit.Constants;
import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;

import org.json.JSONException;

import java.util.UUID;

public class PebbleReceiver extends BroadcastReceiver{
    private static final String TAG = PebbleReceiver.class.getName();

    public void obeyCommand(Context context, PebbleDictionary data) {
        Long action = data.getInteger(CommandDoorService.APP_DATA_KEY);
        Log.d(TAG, "receiveData: " + action);
        Intent intent = new Intent(context, CommandDoorService.class);
        String text;
        if (action == 1) {
            intent.putExtra(CommandDoorService.ACTION, CommandDoorService.OPEN);
            text = context.getString(R.string.opening);
        } else if (action == 2) {
            intent.putExtra(CommandDoorService.ACTION, CommandDoorService.CLOSE);
            text = context.getString(R.string.closing);
        } else {
            //get state and return it
            text = context.getString(R.string.gettingState);
            intent.putExtra(CommandDoorService.ACTION, CommandDoorService.STATE);
        }
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        context.startService(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Constants.INTENT_APP_RECEIVE)) {
            final UUID receivedUuid = (UUID) intent.getSerializableExtra(Constants.APP_UUID);

            // Pebble-enabled apps are expected to be good citizens and only inspect broadcasts containing their UUID
            if (!ProtectedValues.pebbleAppUUID.equals(receivedUuid.toString())) {
                Log.d(TAG, "onReceive: not my UUID");
                return;
            }

            final int transactionId = intent.getIntExtra(Constants.TRANSACTION_ID, -1);
            final String jsonData = intent.getStringExtra(Constants.MSG_DATA);
            if (jsonData == null || jsonData.isEmpty()) {
                Log.d(TAG, "onReceive: jsonData null");
                return;
            }

            try {
                final PebbleDictionary data = PebbleDictionary.fromJson(jsonData);
                obeyCommand(context, data);
                // do what you need with the data
                PebbleKit.sendAckToPebble(context, transactionId);
            } catch (JSONException e) {
                Log.d(TAG, "onReceive: failed reived -> dict" + e);
                return;
            }
        }
    }
}
