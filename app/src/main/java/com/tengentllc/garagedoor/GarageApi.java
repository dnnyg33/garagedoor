package com.tengentllc.garagedoor;

import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by dgeorge on 2/6/16.
 */
public class GarageApi {



    private WebiopiService retrofit;
    private String ClassTag = GarageApi.class.getSimpleName();
    private String responseString;
    public String logs;

    private void createClient() {
        //Current Request
//Get response of the request
/** DEBUG STUFF */ //I am logging the response body in debug mode. When I do this I consume the response (OKHttp only lets you do this once) so i have re-build a new one using the cached body
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request().newBuilder().addHeader("Authorization", ProtectedValues.getAuthString()).build(); //Current Request
                Response response = chain.proceed(originalRequest); //Get response of the request
                /** DEBUG STUFF */
                if (BuildConfig.DEBUG) {
                    //I am logging the response body in debug mode. When I do this I consume the response (OKHttp only lets you do this once) so i have re-build a new one using the cached body
                    String bodyString = response.body().string();
                    Log.d(ClassTag, String.format("Sending request %s with headers %s ", originalRequest.url(), originalRequest.headers()));
                    responseString = String.format("Got response HTTP %s %s with body %s \n\n with headers %s ", response.code(), response.message(), bodyString, response.headers())
                            + responseString;
                    Log.d(ClassTag, responseString);
                    logs = responseString;

                    response = response.newBuilder().body(ResponseBody.create(response.body().contentType(), bodyString)).build();
                }
                return response;
            }
        };

        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(interceptor);

        retrofit = new Retrofit.Builder()
//                .baseUrl("http://192.168.1.17:8000")
                .baseUrl("http://tengentllc.com:8000")
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(WebiopiService.class);
    }

    private static GarageApi ourInstance = new GarageApi();

    public static GarageApi getInstance()
    {
        return ourInstance;
    }

    public static WebiopiService getApi() {
        if (ourInstance == null) {
            ourInstance = new GarageApi();
        }
        return ourInstance.retrofit;
    }

    private GarageApi() {
        createClient();
    }
}
