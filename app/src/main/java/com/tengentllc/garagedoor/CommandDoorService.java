package com.tengentllc.garagedoor;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by dgeorge on 4/9/16.
 */
public class CommandDoorService extends IntentService{

    private static final String TAG = CommandDoorService.class.getCanonicalName();
    public static final int NOTIFICATION_ID = 1233;
    public static final int APP_DATA_KEY = 10;
    public static final String ACTION = "action";
    public static final String OPEN = "open";
    public static final String CLOSE = "close";
    public static final String STATE = "state";
    private NotificationManager notificationManager;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public CommandDoorService(String name) {
        super(name);
    }

    public CommandDoorService() {
        super(TAG);
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        notificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);

        final String action = intent.getStringExtra(ACTION);

        Callback callback = new Callback<DoorAction>() {
            @Override
            public void onResponse(Response<DoorAction> response, Retrofit retrofit) {
                //in either case, clear the notification
                notificationManager.cancel(NOTIFICATION_ID);
                if (!response.isSuccess()) {
                    //if unsuccessful, display new notification
                    GeofenceTransitionsIntentService.buildNotification(getApplicationContext(), action + " failed", getString(R.string.tryAgain), null);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                GeofenceTransitionsIntentService.buildNotification(getApplicationContext(), action + " failed", getString(R.string.tryAgain), null);
            }
        };

        String text;
        switch (action) {
            case OPEN:
                openDoor(callback);
                text = getString(R.string.opening);
                break;
            case CLOSE:
                closeDoor(callback);
                text = getString(R.string.closing);
                break;
            case STATE:
                getState(callback);
                text = getString(R.string.gettingState);
                break;
            default:
                text = "Unknown";
                break;
        }

        //cancel the geofence notification and display loading
        notificationManager.cancel(NOTIFICATION_ID);
        GeofenceTransitionsIntentService.buildNotification(getApplicationContext(), text, "", null);

    }

    private void getState(Callback callback) {
        GarageApi.getApi().getState().enqueue(callback);
    }

    private void closeDoor(Callback callback) {
        //open door
        GarageApi.getApi().closeDoor().enqueue(callback);
    }

    private void openDoor(Callback callback) {
        //open door
        GarageApi.getApi().openDoor().enqueue(callback);
    }
}
