package com.tengentllc.garagedoor;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v7.app.NotificationCompat;

import com.getpebble.android.kit.PebbleKit;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class GeofenceTransitionsIntentService extends IntentService {
    private static final String TAG = GeofenceTransitionsIntentService.class.getSimpleName();
    private static final String ACTION_OPEN = "com.tengentllc.garagedoor.action.open";

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            buildOpenNotification(getApplicationContext(), "Open your garage");
        }
    }

    private static void buildOpenNotification(Context context, String contentText) {
        //create intent, wrapped in pending intent to open the door
        Intent openIntent = new Intent(context, CommandDoorService.class);
        openIntent.putExtra(CommandDoorService.ACTION, CommandDoorService.OPEN);
        PendingIntent pendingOpenIntent = PendingIntent.getService(context, 0, openIntent, 0);
        NotificationCompat.Action openAction = new NotificationCompat.Action.Builder(R.drawable.ic_action_upload, "Open Door", pendingOpenIntent).build();

        buildNotification(context, "You're Home", contentText, openAction);

    }

    public static void buildNotification(Context context, String title, String contentText, android.support.v4.app.NotificationCompat.Action openAction) {
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.house_icon)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentTitle(title)
                .setContentText(contentText);
        if (openAction != null) {
            builder.addAction(openAction);
            builder.setStyle(new NotificationCompat.MediaStyle()
                    .setShowActionsInCompactView(0));
            if (PebbleKit.isWatchConnected(context)) {
                builder.extend(new android.support.v4.app.NotificationCompat.WearableExtender().addAction(openAction));
            }
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(CommandDoorService.NOTIFICATION_ID, builder.build());
    }

}
