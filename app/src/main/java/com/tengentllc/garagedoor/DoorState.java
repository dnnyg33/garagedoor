package com.tengentllc.garagedoor;

/**
 * Created by dgeorge on 2/2/16.
 */
public class DoorState {
    public boolean closed;


    public String currentState() {
        return closed ? "Closed" : "Open";
    }

}
