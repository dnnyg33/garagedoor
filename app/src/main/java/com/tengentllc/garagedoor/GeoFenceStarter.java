package com.tengentllc.garagedoor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GeoFenceStarter extends BroadcastReceiver {
    public GeoFenceStarter() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent("com.tengentllc.garagedoor.GeofenceTransitionsIntentService");
        i.setClass(context, GeofenceTransitionsIntentService.class);
        context.startService(i);
    }
}
